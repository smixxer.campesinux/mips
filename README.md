# MIPS

Simulazione C++ per Modellistica Impianti e Sistemi


## Getting started

Execute "make" to build the application binaries, then use scripts in the bin/ folder to run it.


## Authors and acknowledgment

Corrado Campisano for MIPS exam.


## License

GPLv3.

