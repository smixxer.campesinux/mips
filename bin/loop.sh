#!/usr/bin/env bash

maxEvt=30000
#for runs in 11 12 13 14 15
#for runs in 16 17 18 19 20
#for runs in 21 22 23 24 25
#for runs in 26 27 28 29 30
for runs in 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30
do 
	#for usrs in 05 10 15 20 25 30 35 40 45 50 55 60
	#for usrs in 55 15 45 30 05 40 60 25 50 10 20 35
	#for usrs in 30 05 40 60 25 50 55 15 45 10 20 35
	for usrs in 30
	do
		echo "running with $usrs users, iteration $runs"
		time ./main $usrs $maxEvt -noDebug > data/users-$usrs-run-$runs.txt
		echo "-------------------------------"
	done 
done 