#!/usr/bin/env bash
usrs=30
maxEvt=30000
echo "running with $usrs users, max events $maxEvt"
time ./main $usrs $maxEvt -noDebug > data/single-$usrs.txt
echo "-------------------------------"
echo