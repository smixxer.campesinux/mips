###################################################################
#compile and link executable
main : obj/main.o
		g++ -g3 -o bin/main obj/main.o -lstdc++
		
###################################################################
#compile MAIN
obj/main.o : src/constants.cpp src/main.cpp obj/CentroDiServizio.o obj/CPU.o obj/Disk.o obj/Terminale.o obj/Calendario.o obj/Misure.o obj/Simulazione.o
		g++ -g3 -o obj/main.o -c src/main.cpp
		
###################################################################
#compile class CentroDiServizio
obj/CentroDiServizio.o : src/constants.cpp src/classes/CentroDiServizio.cpp src/headers/CentroDiServizio.h
		g++ -g3 -o obj/CentroDiServizio.o -c src/classes/CentroDiServizio.cpp

###################################################################
#compile class CPU
obj/CPU.o : src/constants.cpp src/classes/CPU.cpp src/headers/CPU.h
		g++ -g3 -o obj/CPU.o -c src/classes/CPU.cpp
		
###################################################################
#compile class Disk
obj/Disk.o : src/constants.cpp src/classes/Disk.cpp src/headers/Disk.h
		g++ -g3 -o obj/Disk.o -c src/classes/Disk.cpp
		
###################################################################
#compile class Terminale
obj/Terminale.o : src/constants.cpp src/classes/Terminale.cpp src/headers/Terminale.h
		g++ -g3 -o obj/Terminale.o -c src/classes/Terminale.cpp
		
###################################################################
#compile class Evento
obj/Evento.o : src/constants.cpp src/classes/Evento.cpp src/headers/Evento.h
		g++ -g3 -o obj/Evento.o -c src/classes/Evento.cpp
	
###################################################################
#compile class Calendario
obj/Calendario.o : src/constants.cpp src/classes/Calendario.cpp src/headers/Calendario.h obj/Evento.o
		g++ -g3 -o obj/Calendario.o -c src/classes/Calendario.cpp
			
###################################################################
#compile class Misure
obj/Misure.o : src/constants.cpp src/classes/Misure.cpp src/headers/Misure.h obj/Misura.o
		g++ -g3 -o obj/Misure.o -c src/classes/Misure.cpp
		
###################################################################
#compile class Misura
obj/Misura.o : src/constants.cpp src/classes/Misura.cpp src/headers/Misura.h
		g++ -g3 -o obj/Misura.o -c src/classes/Misura.cpp

###################################################################
#compile class Simulazione
obj/Simulazione.o : src/constants.cpp src/classes/Simulazione.cpp src/headers/Simulazione.h
		g++ -g3 -o obj/Simulazione.o -c src/classes/Simulazione.cpp		
		
###################################################################
#make ALL
all :
		${MAKE} main
		
###################################################################
#make CLEAN
clean :
		rm bin/main obj/main.o 
		rm obj/CentroDiServizio.o 
		rm obj/CPU.o obj/Disk.o obj/Terminale.o
		rm obj/Calendario.o obj/Evento.o
		rm obj/Misura.o obj/Misure.o
		rm obj/Simulazione.o
