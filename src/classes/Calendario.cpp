#include <iostream>
#include <iomanip>
#include <vector>
#include <string>

using namespace std;

#include "../headers/Calendario.h"
#include "../headers/Misure.h"
#include "../headers/Simulazione.h"

#include "../constants.cpp"



Calendario::Calendario(Simulazione* sim, int iDbg)
{
	simulaz = sim;
	iDebug = iDbg;
	
	currClock = 0;
	
	if (iDebug)	
		cout << S_DEBUG << "Creating Calendario object.";
}
Calendario::~Calendario()
{
	if (iDebug)	
		cout << S_DEBUG << "Simulazione object destroyed.";
}



// TODO : remove these test implementations

//---------------------------
void Calendario::testCalendario(long iMaxEvents, CentroDiServizio* source)
{
	cout << S_TESTS << "Calendario::testCalendario()";
	
	int iMax = rand() % iMaxEvents;
	if (iMax<I_PARAM_MIN_EVENTS) iMax=I_PARAM_MIN_EVENTS;
	
	eventi.clear();
	// TODO : understand WHY if not doing reserve() the vector will mess up with pointers...
	eventi.reserve(iMax);
	
	cout << S_TESTS << "Will create "<< iMax << " events using source '" << source->getName() << "'";
	
	for (int i=1; i<=iMax; i++)
	{
		cout << S_TESTS << "Calendar now holds "<< eventi.size() << " events, adding item n. " << i << " of " << iMax;
		
		double rnd = getRandomForRoute();
		long clk = rand() % 1000;
		CentroDiServizio* dest = source->getNextRoute(rnd);
		Evento* ev = new Evento(source, dest, 0, clk);

		addEvent(ev);
		
		//cout << S_TESTS << "Event to add is :\n\t";
		//ev->printEventShort();
	}
	
	printCalendario();
}

//---------------------------
void Calendario::testEventi(int iUsers, long iMaxEvents, CentroDiServizio* source)
{
	cout << S_TESTS << "Calendario::testEventi()";
	
	// TODO : valutare se ripristinare l'uso del parametro iMaxEvents
	// TODO : oppure se mantenere l'intervallo fisso a
	// TODO : (I_PARAM_MIN_EVENTS, I_PARAM_MAX_EVENTS)
	//int iMax = rand() % iMaxEvents;
	//if (iMax<I_PARAM_MIN_EVENTS) iMax=I_PARAM_MIN_EVENTS;
	// intervallo fisso a (I_PARAM_MIN_EVENTS, I_PARAM_MAX_EVENTS)
	//int iMax = I_PARAM_MIN_EVENTS + rand() % (I_PARAM_MAX_EVENTS - I_PARAM_MIN_EVENTS);
	// intervallo proporzionale al numero di eventi, per compensare le misure skipped
	int iLower = static_cast<int>(I_PARAM_MIN_EVENTS*(log(iUsers)));
	int iUpper = static_cast<int>(I_PARAM_MAX_EVENTS*(log(iUsers)));
	cout << S_TESTS << "Event number range is : ( " << iLower << ", " << iUpper << " )";
	
	int iMax = iLower + rand() % (iUpper-iLower);
	// TODO : 
	iMax = 60000;
	
	eventi.clear();
	// TODO : understand WHY if not doing reserve() the vector will mess up with pointers...
	eventi.reserve(iMax+iUsers);
	
	cout << S_TESTS << "Will start creating " << iUsers << " events from initial source '" << source->getName() << "'";
	cout << S_TESTS << "\tThis is just because we have " << iUsers << " indipendent users on '" << source->getName() << "'";
	
	for (int i=1; i<=iUsers; i++)
	{
		long clk = source->getNextEventClock();
		
		CentroDiServizio* dest = source->getNextRoute(getRandomForRoute());
		Evento* ev = new Evento(source, dest, 0, clk);
		
		addEvent(ev);
		
		//cout << S_TESTS << "Event to add is :\n\t";
		//ev->printEventShort();
	}
	printCalendario();
	
	// look for the latest event
	long edge = getLastEventClock();
	cout << S_ALERT << "Setting edge to Measure objects to clock : " << edge << " since it's just 1 clock after last Terminale user got in.";
	simulaz->setEdge(edge);
	
	
	cout << S_TESTS << "Will continue creating up to " << iMax << " new events by processing existing events...";
	
	int iCount = 1;
	
	Evento* nextEvent = getNextEventToProcess(currClock, 0);
	while (nextEvent!=0 && iCount<iMax)
	{
		//cout << S_TESTS << "Iteration \t : " << iCount;
		//cout << S_TESTS << "Event to process is :\n\t";
		//nextEvent->printEventShort();
		
		processEvent(nextEvent, 0);
		
		//printCalendario();
		
		nextEvent = getNextEventToProcess(currClock, 0);
		
		iCount++;
	}
	
	printCalendario();
}
// TODO : end remove...



void Calendario::addEvent(Evento* ev)
{
	ev->setId(getNewEventId());
	
	if (iDebug)	
		cout << S_DEBUG << "vector<Evento>.capacity() : " << eventi.capacity();
	
	long pos=1;
	if (pos>eventi.size())
	{
		if (iDebug)	
			cout << S_DEBUG << "Adding first Event to empty Calendar.";
		eventi.insert(eventi.begin(), 1, *ev);
	}
	else
	{
		vector<Evento>::iterator iterator = eventi.begin();
		while (iterator!=eventi.end())
		{
			if (iDebug)	
				cout << S_DEBUG << "Checking Calendar calendar item " << pos << " of " << eventi.size();
				
			Evento curr = eventi.at(pos-1);
			if (iDebug)	
			{
				cout << S_DEBUG << "Event to check is :\n\t";
				curr.printEventShort();
			}
			
			if (curr.getClock()>ev->getClock())
			{
				if (iDebug)	
					cout << S_DEBUG << "Adding item before current";
				
				//iter.
				eventi.insert(iterator, 1, *ev);
				break;
			}
			else 
			{
				if (curr.getClock()==ev->getClock())
				{
					
					cout << S_ALERT << "Whish to add item just after current as both have same clock : " << ev->getClock(); 
					cout << S_ALERT << "\tWill try increasing clock for the item yet to be added, using Event::raiseClock().";
					
					ev->raiseClock();
					cout << S_TESTS << "Event to add is now :\n\t";
					ev->printEventShort();
				}
				
				if (pos<eventi.size())
				{
					if (iDebug)	
						cout << S_DEBUG << "Must check next item";
				}
				else 
				{
					if (iDebug)	
						cout << S_DEBUG << "Adding item at the end";
					eventi.insert(eventi.end(), 1, *ev);
					break;
				}
			}
			
			iterator++;
			pos++;
		}
	}
}



Evento* Calendario::getNextEventToProcess(long clock, long skipCount)
{
	vector<Evento>::iterator iterator = eventi.begin();
	long pos = 1;
	while (iterator!=eventi.end())
	{
		Evento* curr = &(*iterator);		
		
		if (curr->getClock()>clock)
		{
			return curr;
			break;
		}
		
		pos++;
		iterator++;
	}
	
	return 0;
}



long Calendario::getLastEventClock()
{
	long ret=0;
	
	vector<Evento>::iterator iterator = eventi.begin();

	while (iterator!=eventi.end())
	{
		Evento* curr = &(*iterator);		
		
		ret = curr->getClock();

		iterator++;
	}

	return ret;
}



// TODO : verify if WORTH to cleanUp ...
void Calendario::processEvent(Evento* event, int iForTest)
{
	// se la SRC e' T     --> aumento il numero di utenti in attesa   --> ADESSO
	// se la SRC non e' T --> diminuisco il numero di job in coda     --> ADESSO
	// trattasi di FUNZIONE VIRTUAL del generico CantroDiServizio
	// pare che debba farlo SEMPRE ADESSO
	if (!(event->getSource()->isTerminalOrCpuDisk()) || true)
		event->getSource()->registerAsSource();
	// se la DST e' T     --> diminuisco il numero di utenti in attesa --> DOPO
	// se la DST non e' T --> aumento il numero di job in coda         --> DOPO
	// trattasi di FUNZIONE VIRTUAL del generico CantroDiServizio
	// pare che debba farlo SEMPRE DOPO
	if ((event->getDestination()->isTerminalOrCpuDisk()) && false)
		event->getDestination()->registerAsDestination();
	
	// aggiorno il clock a quello dell'evento processato
	currClock = event->getClock();
	
	// per Terminale devo campionare il tempo complessivo di risposta
	if ((event->getDestination()->isTerminalOrCpuDisk()))
	{
		// TODO : IMPLEMENT REAL FUNCTION !!!
		long totalClock = currClock - getClockFromRootEvent(event);
		
		Misura* m = new Misura(currClock, totalClock);
		simulaz->getMisureRisposta()->setNext(*m);
		
		if (iForTest)
			cout << S_TESTS << "Adding Measurement (" << currClock << " , " << totalClock << ") to '" << simulaz->getMisureRisposta()->getName() << "'";
			
	}
	
	
	/*
	if( event->getId()==21057 || event->getId()==21065 || event->getId()==21069 || event->getId()==21072 )
	{
		printCalendarioHeader();
		printCalendario();
		cout << S_ALERT << "ERROR HERE";
		event->printEventShort();
		cout << S_ALERT << "ERROR HERE";
	}
	*/
	
	
	// imposto come SRC del prossimo evento, la DST dell'evento da processare
	CentroDiServizio* nextSrc = event->getDestination();
	// chiedo alla SRC del prossimo evento quale sara' la sua DST, in base al routing
	CentroDiServizio* nextDest = nextSrc->getNextRoute(getRandomForRoute());
	
	if (iForTest)
		cout << S_TESTS << "Current DST '" << event->getDestination()->getName() << "' will route to future DST " << nextDest->getName() << "'.";
	
	// chiedo alla SRC del prossimo evento, quanto tempo restera' occupata su di esso 
	// sara' il tempo di generaz. di comando su T o il tempo di servizio su Cpu e DiskX 	
	long nextDelay = nextSrc->getNextEventClock();
	if (iForTest)
		cout << S_TESTS << "Current DST '" << nextSrc->getName() << "' will spend " << nextDelay << " clocks on event to be added";
		
	// calcolo il clock a cui si verifichera' il prox evento:
	// recupero il clock futuro in cui la SRC del prossimo evento sara' libera:
	long nextFreeTime = getWhenWillBeFree(nextSrc, event, iForTest);
	
	if (nextFreeTime==-1)
	{
		// non posso aggiungere l'evento... se e' un terminale poco male, ma se e' CPU o DiskX e' un problema!
		if (iForTest)
			cout << S_ALERT << "Will NOT add any Event";
	}
	else
	{
		// calcolo il tempo di attesa in coda
		long lQueueWaitingTime = nextFreeTime-currClock;
		if (iForTest)
			cout << S_TESTS << "Event to be added will WAIT " << lQueueWaitingTime << " within '" << nextSrc->getName() << "' queue";
		
	
		// per Disk1 devo campionare il tempo di attesa in coda
		if (simulaz->getMisureAttesa()->isOnCds(nextSrc))
		{
			if (lQueueWaitingTime>1)
			{
				Misura* m = new Misura(currClock, lQueueWaitingTime);
				simulaz->getMisureAttesa()->setNext(*m);
				
				if (iForTest)
					cout << S_TESTS << "Adding Measurement (" << currClock << " , " << lQueueWaitingTime << ") to '" << simulaz->getMisureAttesa()->getName() << "'";
			}
			else 
			{
				if (iForTest)
					cout << S_TESTS << "Skipping Measurement (" << currClock << " , " << lQueueWaitingTime << ") to '" << simulaz->getMisureAttesa()->getName() << "'";
			}
		}
			
		// e lo sommo al tempo che essa impieghera' sul prox evento
		long nextClock = nextFreeTime + nextDelay;
		
		
		Evento* nextEvent;
		// creo il nuovo oggetto evento
		if ((event->getDestination()->isTerminalOrCpuDisk()))
		{
			// per Terminale devo RESETTARE il PARENT
			nextEvent = new Evento(nextSrc, nextDest, 0, nextClock);
		}
		else 
		{
			// per gli altri (CPU,DiskX) come al solito 
			nextEvent = new Evento(nextSrc, nextDest, event, nextClock);
		}
		
		
		// se la SRC e' T     --> aumento il numero di utenti in attesa   --> PRIMA
		// se la SRC non e' T --> diminuisco il numero di job in coda     --> PRIMA
		// trattasi di FUNZIONE VIRTUAL del generico CantroDiServizio
		// pare che debba farlo SEMPRE PRIMA
		if (!(event->getSource()->isTerminalOrCpuDisk()) && false)
			event->getSource()->registerAsSource();
		// se la DST e' T     --> diminuisco il numero di utenti in attesa --> ADESSO
		// se la DST non e' T --> aumento il numero di job in coda         --> ADESSO
		// trattasi di FUNZIONE VIRTUAL del generico CantroDiServizio
		// pare che debba farlo SEMPRE ADESSO
		if ((event->getDestination()->isTerminalOrCpuDisk()) || true)
			event->getDestination()->registerAsDestination();
		
		
		// aggiungo l'evento al calendario, mantenendolo ordinato
		addEvent(nextEvent);
		
		
		if (iDebug)
		{
			cout << S_DEBUG << "Event to add is :\n\t";
			nextEvent->printEventShort();
		}
		if (iForTest)
		{
			cout << S_TESTS << "Event to add is :\n\t";
			nextEvent->printEventShort();
		}
	}
}



long Calendario::getNewEventId()
{
	return eventi.size()+1;
}



// TODO : verify if WORTH to cleanUp ...
long Calendario::getWhenWillBeFree(CentroDiServizio* nextSrc, Evento* processedEvent, int iForTest)
{
	
	/*
	if( processedEvent->getId()==21057 || processedEvent->getId()==21065 || processedEvent->getId()==21069 || processedEvent->getId()==21072 )
	{
		printCalendarioHeader();
		printCalendario();
		cout << S_ALERT << "ERROR HERE";
		processedEvent->printEventShort();
		cout << S_ALERT << "ERROR HERE";
	}
	*/
	
	string nextSrcName = nextSrc->getName();
	
	// sicuramente e' almeno dopo il clock attuale
	long lWhen = currClock;
	
	// recupero il numero di:
	// job in coda per CPU o DiskX
	// utenti in attesa per T 
	long lQueuedCount = nextSrc->getQueuedOrWaiting();
	
	// il TERMINALE e' gestito come se NON avesse CODA (e in effetti...)
	if (lQueuedCount==0 || nextSrc->isTerminalOrCpuDisk())
	{
		// aggiungo +1 solo per evitare l'alert in inserimento
		lWhen++;
		
		if (iForTest)
		{
			if (lQueuedCount==0)
				cout << S_TESTS << "Current DST '" << nextSrcName << "' has nothing to wait for";
			if (nextSrc->isTerminalOrCpuDisk())
				cout << S_TESTS << "Current DST '" << nextSrcName << "' is TERMINAL: will restart early";
		}
		
	}
	else
	{
		if (iForTest)
			cout << S_TESTS << "Current DST '" << nextSrcName << "' has " << lQueuedCount << " job queued to be ex. or command sent to be ret.";
		
		// recupero il clock a cui essa sara' libera dagli N elementi nella sua coda
		//
		// ricordo che la gestione e' FIFO, per cui, se in futuro verra'
		// richiesto un altro impegno alla DST, questo finira' comunque
		// DOPO quello attuale, mai prima, e il tempo calcolato adesso 
		// restera' comunque valido. Inoltre gli eventi futuri sulla DST
		// terranno conto anche di questo che vado ad aggiungere
		// 
		// per questo recupero il clock dell'evento futuro in cui:
		// se la DST e' T     --> e' DST di N eventi
		// se la DST non e' T --> e' SRC di N eventi
		
		// scorro il calendario
		vector<Evento>::iterator iterator = eventi.begin();
		long pos = 1;
		long lEventsFound = 0;
		long currPos = 0;
		while (iterator!=eventi.end())
		{
			Evento* evt = &(*iterator);		
			
			// all'inizio non ho trovato nulla...
			if (currPos>0)
			{
				// se al ciclo precedente ho trovato l'evento al clock corrente
				
				// vedo di trovare N eventi successivi
				// se la DST e' T     --> e' DST di N eventi
				// se la DST non e' T --> e' SRC di N eventi
				string evtCdsName = "";
				if (nextSrc->isTerminalOrCpuDisk())
				{
					evtCdsName = evt->getDestination()->getName();
				}
				else 
				{
					evtCdsName = evt->getSource()->getName();
				}
				
				if ( !(evtCdsName).compare(nextSrcName) )
				{
					lEventsFound++;
					
					if (iForTest)
						cout << S_TESTS << "Found " << lEventsFound << " of " << lQueuedCount 
							<< " future event for '" << nextSrcName << "' at pos. " << pos 
							<< " and clock " << evt->getClock();
				}
				
				// se ho trovato tanti eventi quanti quelli in coda
				if (lEventsFound==lQueuedCount)
				{
					// allora il clock cercato e' quello dell'evento
					lWhen = evt->getClock();
					break;
				}
			}
			else
			{
				// se non ho ancora trovato l'evento al clock corrente
				if (evt->getClock()==currClock)
				{
					currPos = pos;
					
					if (iForTest)
						cout << S_TESTS << "Current clock event found at position : " << currPos;
				}
			}
			
			pos++;
			iterator++;
		}
		
		if (lEventsFound!=lQueuedCount)
		{
			// returning code-break value
			lWhen = -1;
			
			if (nextSrc->isTerminalOrCpuDisk())
			{
				cout << S_ALERT << " MODERATE : couldn't find enough future events for '" << nextSrcName << "'";
			}
			else 
			{
				cout << S_ALERT << " GRAVE : couldn't find enough future events for '" << nextSrcName << "', was processing\n\t";
				processedEvent->printEventShort();
			}
		}
		
	}
	
	return lWhen;
}



long Calendario::getClockFromRootEvent(Evento* evt)
{
	long ret = currClock;
	
	// make COPY to avoid write messes...
	Evento* current = new Evento(*evt);
	
	Evento* parent = current->getParent();
	while(parent!=0)
	{
		//cout << S_TESTS << "Parent event FOUND for event ID: " << current->getId() << " as FK: " << parent->getId();
		
		ret = parent->getClock();
		
		current = parent;
		parent = current->getParent();
	}
	
	return ret;
}


void Calendario::printCalendario()
{
	printCalendarioHeader();
	Evento::printEventHeader();
		
	vector<Evento>::iterator iterator = eventi.begin();
	long pos = 1;
	while (iterator!=eventi.end())
	{
		// ... solo per giocare con & e * ...
		//Evento* curr = new Evento(*iterator);
		//curr->printEvent(pos);
		Evento* curr = &(*iterator);		
		curr->printEvent(pos);
		
		pos++;
		iterator++;
	}
}


void Calendario::printCalendarioHeader()
{
	cout << S_PRINT << "-------------- CALENDARIO status at clock : " << currClock << " --------------";
}
