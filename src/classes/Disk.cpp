#include <iostream>
#include <string>

using namespace std;

#include "../headers/Disk.h"

#include "../constants.cpp"



Disk::Disk(int iDbg, string n, double distrParam, double distrSubParam)
{
	isTerminal = 0;
	
	dDistrParam = distrParam;
	dDistrSubParam = distrSubParam;
	
	iDebug = iDbg;
	name = n;
	
	if (iDebug)	
		cout << S_DEBUG << "Disk object '" << name << "' created.";
}
Disk::~Disk()
{
	if (iDebug)	
		cout << S_DEBUG << "Disk object '" << name << "' destroyed.";
}


void Disk::manageEvent()
{
	if (iDebug)	
		cout << S_DEBUG << "Disk object '" << name << "' managing event 'End of " << name << " I/O Access'.";
}


long Disk::getNextEventClock()
{
	if (iDebug)	
			cout << S_DEBUG << "Executing virtual function Disk::getNextEventClock";
	
	int kappa = (int) dDistrSubParam;
	
	double rnd = getRandomKappaErlang(dDistrParam, kappa);
	
	long clk = getRandomToClockScaled(rnd);
	
	return clk;
}


void Disk::registerAsSource()
{
	if (iDebug)
			cout << S_DEBUG << "Executing virtual function Disk::registerAsSource";
	iQueued--;
}
void Disk::registerAsDestination()
{
	if (iDebug)
			cout << S_DEBUG << "Executing virtual function Disk::registerAsDestination";
	iQueued++;
}
