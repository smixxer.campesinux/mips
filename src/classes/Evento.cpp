#include <iostream>
#include <iomanip>
#include <string>

using namespace std;

#include "../headers/Evento.h"

#include "../constants.cpp"



Evento::Evento(CentroDiServizio* src, CentroDiServizio* dst, Evento* prt, long clk, long idd)
{
	source = src;
	destination = dst;
	
	parent = prt;
	
	clock = clk;
	
	id=idd;
}
Evento::Evento(const Evento &evt)
{
	source = evt.source;
	destination = evt.destination;
	
	parent = evt.parent;
	
	clock = evt.clock;
	
	id = evt.id;
}

Evento::~Evento()
{
}



CentroDiServizio* Evento::getSource()
{
	return source;
}


CentroDiServizio* Evento::getDestination()
{
	return destination;
}


Evento* Evento::getParent()
{
	return parent;
}
void Evento::setParent(Evento* prt)
{
	parent = prt;
}


long Evento::getClock()
{
	return clock;
}
void Evento::raiseClock()
{
	clock++;
}


long Evento::getId()
{
	return id;
}
void Evento::setId(long idd)
{
	id = idd;
}


void Evento::printEventHeader()
{
	cout.setf(ios::left, ios::adjustfield);
	
	cout << S_PRINT;
	cout << setw(10) << setfill(' ') << "Position";
	cout << setw(10) << setfill(' ') << "EventId";
	cout << setw(10) << setfill(' ') << "ParentId";
	cout << setw(10) << setfill(' ') << "Source";
	cout << setw(5) << setfill(' ') << "SrcQ";
	cout << setw(10) << setfill(' ') << "Destinat.";
	cout << setw(5) << setfill(' ') << "DstQ";
	cout << setw(15) << setfill(' ') << "Clock";
}
void Evento::printEvent(long pos)
{
	cout.setf(ios::left, ios::adjustfield);
	
	cout << S_PRINT;
	cout << setw(10) << setfill(' ') << pos;
	cout << setw(10) << setfill(' ') << getId();
	if (getParent()>0)
	{
		Evento* parent = getParent();
		long fk = parent->getId();
		cout << setw(10) << setfill(' ') << fk;
	}
	else
	{
		cout << setw(10) << setfill(' ') << S_EVENT_NO_PARENT;
	}
	cout << setw(10) << setfill(' ') << getSource()->getName();
	cout << setw(5) << setfill(' ') << getSource()->getQueuedOrWaiting();
	cout << setw(10) << setfill(' ') << getDestination()->getName();
	cout << setw(5) << setfill(' ') << getDestination()->getQueuedOrWaiting();
	cout << setw(15) << setfill(' ') << getClock();
}
void Evento::printEventShort(long pos)
{
	if (pos>0)
		cout << "Position: " << pos << ", ";
	
	cout << "ID: " << getId() << ", FK: ";
	
	if (getParent()>0)
	{
		Evento* parent = getParent();
		long fk = parent->getId();
		cout << fk;
	}
	else
	{
		cout << S_EVENT_NO_PARENT_SHORT;
	}
	
	cout << ", Source: '" << getSource()->getName() << "', Destination: '" << getDestination()->getName() << "', Clock: " << getClock();
}
