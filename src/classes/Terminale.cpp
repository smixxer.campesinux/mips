#include <iostream>
#include <string>

using namespace std;

#include "../headers/Terminale.h"

#include "../constants.cpp"



Terminale::Terminale(int iDbg, string n, double distrParam, double distrSubParam)
{
	isTerminal = 1;
	
	dDistrParam = distrParam;
	dDistrSubParam = distrSubParam;
	
	iDebug = iDbg;
	name = n;
	
	if (iDebug)	
		cout << S_DEBUG << "Terminale object '" << name << "' created.";
}
Terminale::~Terminale()
{
	if (iDebug)	
		cout << S_DEBUG << "Terminale object '" << name << "' destroyed.";
}


void Terminale::manageEvent()
{
	if (iDebug)	
		cout << S_DEBUG <<"Terminale object '" << name << "' managing event 'Arrival of " << name << " Job'.";
}


long Terminale::getNextEventClock()
{
	if (iDebug)	
			cout << S_DEBUG << "Executing virtual function Terminale::getNextEventClock";
	
	double rnd = getRandomExponential(dDistrParam);
	
	long clk = getRandomToClockScaled(rnd);
	
	return clk;
}


void Terminale::registerAsSource()
{
	//if (iDebug)
	//		cout << S_DEBUG << "Executing virtual function Terminale::registerAsSource";
	iQueued++;
}
void Terminale::registerAsDestination()
{
	//if (iDebug)
	//		cout << S_DEBUG << "Executing virtual function Terminale::registerAsDestination";
	iQueued--;
}
