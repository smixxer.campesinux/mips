#include <iostream>
#include <queue>
#include <iomanip>
#include <string>

using namespace std;

#include "../headers/Misure.h"

#include "../constants.cpp"

#include "Misura.cpp"



Misure::Misure(CentroDiServizio* c, string name, int iDbg)
{
	iDebug = iDbg;
	
	if (iDebug)	
		cout << S_DEBUG << "Creating Misure object.";
	
	cds = c;
	measureName = name;
	
	lEdge = 0;
	
	count = 0;

	if (iDebug)
		cout << S_DEBUG << "Misure object created for '" << measureName << "' on '" << c->getName() << "'";
	
}
Misure::~Misure()
{
}


void Misure::testMisure(long j)
{
	cout << S_TESTS << "Misure object '" << getName() << "' will hold " << j << " elements.";
	for (int i=0; i<j; i++) {
		Misura misura(i+1, getForTestRandomMisura());
		setNext(misura);
	}
	
	// printing data
	printMisure();
}


Misura Misure::getNext()
{
	Misura tmp;
	if (count>0)
	{
		tmp = misure.front();
	    misure.pop();
	    count --;
	}
	return tmp;
}


void Misure::setNext(Misura next)
{
	misure.push(next);
	count ++;
	
	if (iDebug)	
	{
		cout << S_DEBUG << "Misure object '" << measureName << "' now holds " << count << " elements.";
		cout << S_DEBUG << "\tLast added is (" << next.getClock() << " , " << next.getValue() << ").";
	}
}


long Misure::getSize()
{
	return count;
}


string Misure::getName()
{
	return measureName;
}


void Misure::setEdge(long edge)
{
	lEdge=edge;
}


CentroDiServizio* Misure::getCds()
{
	return cds;
}


int Misure::isOnCds(CentroDiServizio* c)
{
	if ( !(c->getName().compare(cds->getName())) )
	{
		return 1;
	}
	{
		return 0;
	}
}


void Misure::printMisure()
{
	cout.setf(ios::left, ios::adjustfield);
	
	long lSkipped=0;
	long lCount = getSize();
	cout << S_PRINT << "Misure object '" << getName() << "' on '" << cds->getName() << "' holds " << lCount << " elements.";
	
	Misure measures(cds, "");
	
	if (lCount>0)
	{
		cout << S_PRINT;
		cout << setw(15) << setfill(' ') << "Clock";
		cout << setw(15) << setfill(' ') << "Valore";
		cout << setw(10) << setfill(' ') << "Skipped";
		
		// TODO : VERIFY
		// non ci credo... non posso aver avuto overflow pe' 'sta stronzata...
		//double med = 0;
		double med = 0.0;
		// TODO : VERIFY
		// non ci credo... non posso aver avuto overflow pe' 'sta stronzata...
		//double var = 0;
		double var = 0.0;
		
		// read and print all the elements from the queue
		// con l'occasione calcolo la media
		for (int i=0; i<lCount; i++) {
			Misura misura = getNext();
			measures.setNext(misura);
			
			cout << S_PRINT;
			cout << setw(15) << setfill(' ') << misura.getClock();
			cout << setw(15) << setfill(' ') << misura.getValue();
			
			if(misura.getClock()>lEdge)
			{
				cout << setw(10) << setfill(' ') << "No";
				// TODO : VERIFY
				// non ci credo... non posso aver avuto overflow pe' 'sta stronzata...
				//med += misura.getValue();
				med += (double) misura.getValue();
			}
			else
			{
				cout << setw(10) << setfill(' ') << "Yes";
				lSkipped++;
			}
		}
		med /= (lCount-lSkipped);
		
		
		// calcolo varianza : devo fare una nuova scansione
		if (lCount-lSkipped>1)
		{
			for (int i=0; i<lCount; i++) {
				Misura misura = measures.getNext();
				
				if(misura.getClock()>lEdge)
				{
					// TODO : VERIFY
					// non ci credo... non posso aver avuto overflow pe' 'sta stronzata...
					//double tmp = (misura.getValue()-med);
					double tmp = ( ((double)misura.getValue()) -med);
					
					// TODO : VERIFY
					// non ci credo... non posso aver avuto overflow pe' 'sta stronzata...
					// ...eppoi qui che sarebbe successo???
					var += tmp*tmp;
				}
			}
			var /= (lCount-lSkipped-1);
		}
		else 
		{
			var = 0;
		}
		
		// la dev.std. e' la RADICE QUADRATA della varianza
		double dev = sqrt(var);
		cout << S_PRINT << "----------------------------------- RISULTATI ---------------------------------";
		cout << S_PRINT << "Misure object '" << getName() << "' on '" << cds->getName() << "' holds " << lCount << " elements.";
		cout << S_PRINT << "-------------------------------------------------------------------------------";
		
		cout << S_PRINT << "----------------STATISTICHE GENERALI------------------";
		cout << S_PRINT;
		cout << setw(10) << setfill(' ') << "Misure";
		cout << setw(10) << setfill(' ') << "Skipped";
		cout << setw(10) << setfill(' ') << "Valide";
		cout << setw(10) << setfill(' ') << "Edge";
		//
		cout << S_PRINT;
		cout << setw(10) << setfill(' ') << lCount;
		cout << setw(10) << setfill(' ') << lSkipped;
		cout << setw(10) << setfill(' ') << (lCount-lSkipped);
		cout << setw(10) << setfill(' ') << lEdge;
		
		cout << S_PRINT << "--------------VALORI IN TERMINI di CLOCK--------------";
		cout << S_PRINT;
		cout << setw(15) << setfill(' ') << "Media";
		cout << setw(15) << setfill(' ') << "Varianza";
		cout << setw(15) << setfill(' ') << "Dev.Std.";
		//
		cout << S_PRINT;
		cout << setw(15) << setfill(' ') << med;
		cout << setw(15) << setfill(' ') << var;
		cout << setw(15) << setfill(' ') << dev;
		
		cout << S_PRINT << "------------VALORI IN TERMINI di MilliSec.------------";
		cout << S_PRINT;
		cout << setw(15) << setfill(' ') << "Media";
		cout << setw(15) << setfill(' ') << "Varianza";
		cout << setw(15) << setfill(' ') << "Dev.Std.";
		//
		cout << S_PRINT;
		cout << setw(15) << setfill(' ') << getClockScaledToMilliSecs(med);
		cout << setw(15) << setfill(' ') << getClockScaledToMilliSecs(getClockScaledToMilliSecs(var));
		cout << setw(15) << setfill(' ') << getClockScaledToMilliSecs(dev);
		
		cout << endl;
		
	}
}
