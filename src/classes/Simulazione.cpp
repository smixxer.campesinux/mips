#include <iostream>
#include <iomanip>
#include <string>

using namespace std;

#include "../headers/Simulazione.h"

#include "../constants.cpp"



double dRouteCpuTerminale = 0.035088;	//0,035088
double dRouteCpuDisk1 = 0.701754;		//0,701754
double dRouteCpuDisk2 = 0.263158;		//0,263158

double dRouteTerminaleCpu = 1.0;

double dRouteDisk1Cpu = 1.0;
double dRouteDisk2Cpu = 1.0;



Simulazione::Simulazione(int iDbg, int iUsr, long lMaxEvts)
{
	iDebug = iDbg;
	iUsers = iUsr;
	lMaxEvents = lMaxEvts;
	
	if (iDebug)	
		cout << S_DEBUG << "Creating Simulazione object.";
		
	setupCdsAndRouting();
	
	setupMisure();
	
	setupCalendario();

	if (iDebug)	
		cout << S_DEBUG << "Simulazione object created.";
	
}
Simulazione::~Simulazione()
{
	if (iDebug)	
		cout << S_DEBUG << "Destroying Simulazione object.";
	
	delete cpu;
	delete disk1;
	delete disk2;
	delete terminale;
	
	if (iDebug)	
		cout << S_DEBUG << "Simulazione object destroyed.";
}



// TODO : remove these test implementations

//-------------------------------------
void Simulazione::testNextRoute()
{
	cout << S_TESTS << "Simulazione::testNextRoute()";
	
	double rnd = 0.0;
	CentroDiServizio* ret;
	
	rnd = getRandomForRoute();
	ret = cpu->getNextRoute(rnd);
	cout << S_TESTS << "Using random: " << rnd << " route from '" << cpu->getName() << "' to '" << ret->getName() << "' was choosen";
	
	rnd = getRandomForRoute();
	ret = disk1->getNextRoute(rnd);
	cout << S_TESTS << "Using random: " << rnd << " route from '" << disk1->getName() << "' to '" << ret->getName() << "' was choosen";
	
	rnd = getRandomForRoute();
	ret = disk2->getNextRoute(rnd);
	cout << S_TESTS << "Using random: " << rnd << " route from '" << disk2->getName() << "' to '" << ret->getName() << "' was choosen";
	
	rnd = getRandomForRoute();
	ret = terminale->getNextRoute(rnd);
	cout << S_TESTS << "Using random: " << rnd << " route from '" << terminale->getName() << "' to '" << ret->getName() << "' was choosen";
	
	cout << endl;
}

//-------------------------------------
void Simulazione::testMisure()
{
	cout << S_TESTS << "Simulazione::testMisure()";
	
	// test misureAttesa
	int iMusureAttesa = rand() % 10;
	misureAttesa->testMisure(iMusureAttesa);
	
	// test misureRisposta
	int iMusureRisposta = rand() % 10;
	misureRisposta->testMisure(iMusureRisposta);
}

//-------------------------------------
void Simulazione::testCalendario()
{
	cout << S_TESTS << "Simulazione::testCalendario()";
	calendario->testCalendario(lMaxEvents, cpu);
}

//-------------------------------------
void Simulazione::testDistros()
{
	cout << S_TESTS << "Simulazione::testDistros()";
	
	testDistro(cpu, "IpExp p=0.3");
	
	testDistro(disk1, "3-erlang");
	
	testDistro(disk2, "2-erlang");
	
	testDistro(terminale, "Esponenz.");
}
void Simulazione::testDistro(CentroDiServizio* cds, string distro)
{
	int iTests = (int) lMaxEvents;
	
	cout.setf(ios::left, ios::adjustfield);
	cout << S_PRINT << "-----------------------------------------------------------------------";
	cout << S_PRINT;
	cout << setw(15) << setfill(' ') << "CentroDiServ.";
	cout << setw(15) << setfill(' ') << "Distribuzione";
	cout << setw(12) << setfill(' ') << "TempoMedio";
	cout << setw(12) << setfill(' ') << "Estrazioni";
	cout << S_PRINT;
	cout << setw(15) << setfill(' ') << cds->getName();
	cout << setw(15) << setfill(' ') << distro;
	cout << setw(12) << setfill(' ') << cds->getDistrParam();
	cout << setw(12) << setfill(' ') << iTests;
	
	cout << endl;
	
	cout.setf(ios::right, ios::adjustfield);
	cout << setw(15) << setfill(' ') << "ClockCount";
	cout << setw(15) << setfill(' ') << "MilliSecs.";
	cout << endl;
	
	long clock = 0;
	double millis = 0.0;
	for (int i=1; i<=iTests; i++)
	{
		clock = cds->getNextEventClock();
		millis = getClockScaledToMilliSecs(clock);
		cout << setw(15) << setfill(' ') << clock;
		cout << setw(15) << setfill(' ') << millis;
		cout << endl;
	}
}
//-------------------------------------
void Simulazione::testEventi()
{
	cout << S_TESTS << "Simulazione::testEventi()";
	calendario->testEventi(iUsers, lMaxEvents, terminale);
	cout << S_TESTS << "END OF Simulazione::testEventi()";
	misureAttesa->printMisure();
	misureRisposta->printMisure();
}

// TODO : end remove...



void Simulazione::setupCdsAndRouting()
{
	if (iDebug)	
		cout << S_DEBUG << "Simulazione::setupCdsAndRouting()";

	cpu = new CPU(iDebug, S_CDS_CPU, D_CDS_MILLISECS_PER_JOB_CPU, D_CDS_DISTRO_PARAM2_CPU);
	
	disk1 = new Disk(iDebug, S_CDS_Disk1, D_CDS_MILLISECS_PER_JOB_Disk1, D_CDS_DISTRO_PARAM2_Disk1);
	disk2 = new Disk(iDebug, S_CDS_Disk2, D_CDS_MILLISECS_PER_JOB_Disk2, D_CDS_DISTRO_PARAM2_Disk2);
	
	terminale = new Terminale(iDebug, S_CDS_Terminale, D_CDS_MILLISECS_PER_JOB_Terminale, D_CDS_DISTRO_PARAM2_Terminale);
	
	cpu->addRouting(disk1, dRouteCpuDisk1);
	cpu->addRouting(disk2, dRouteCpuDisk2);
	cpu->addRouting(terminale, dRouteCpuTerminale);
	cpu->printRoutings();
	
	disk1->addRouting(cpu, dRouteDisk1Cpu);
	disk1->printRoutings();
	
	disk2->addRouting(cpu, dRouteDisk2Cpu);
	disk2->printRoutings();
	
	terminale->addRouting(cpu, dRouteTerminaleCpu);
	terminale->printRoutings();
}


void Simulazione::setupMisure()
{
	if (iDebug)	
		cout << S_DEBUG << "Simulazione::setupMisure()";
	
	misureAttesa = new Misure(disk1, S_MISURE_ATTESA, iDebug);
	misureRisposta = new Misure(terminale, S_MISURE_RISPOSTA, iDebug);
}


void Simulazione::setupCalendario()
{
	if (iDebug)	
		cout << S_DEBUG << "Simulazione::setupCalendario()";
	calendario = new Calendario(this, iDebug);
}


Misure* Simulazione::getMisureAttesa()
{
	return misureAttesa;
}
Misure* Simulazione::getMisureRisposta()
{
	return misureRisposta;
}


void Simulazione::setEdge(long edge)
{
	misureAttesa->setEdge(edge);
	misureRisposta->setEdge(edge);
}
/*
void Simulazione::manageCpuEvent()
{
	cpu->manageEvent();
}
void Simulazione::manageDisk1Event()
{
	disk1->manageEvent();
}
void Simulazione::manageDisk2Event()
{
	disk2->manageEvent();
}
void Simulazione::manageTerminalEvent()
{
	terminale->manageEvent();
}
*/
