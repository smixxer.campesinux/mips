#include <iostream>
#include <string>

using namespace std;

#include "../headers/Misura.h"



// TODO : verificare se vale la pena/e' possibile fare un Template di classe...
// TODO : per ora si usa LONG per il valore della misura, visto che devo misurare:
// - il tempo di attesa in coda (differenza di "clock")
// - il tempo totale di esecuzione (sommatoria di "clock")
Misura::Misura(long clk, long val)
{
	clock = clk;
	value = val;
}
Misura::~Misura()
{
}



long Misura::getClock() {
	return clock;
}


long Misura::getValue() {
	return value;
}
