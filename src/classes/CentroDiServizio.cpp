#include <iostream>
#include <iomanip>
#include <string>

using namespace std;

#include "../headers/CentroDiServizio.h"

#include "../constants.cpp"



CentroDiServizio::CentroDiServizio(int iDbg, string n, double distrParam, double distrSubParam)
{
	iDebug = iDbg;
	
	name = n;
	iQueued=0;
	
	destinations.clear();
	probabilities.clear();
	
	if (iDebug)	
		cout << S_DEBUG << "CentroDiServizio object '" << name << "' created.";
}
CentroDiServizio::~CentroDiServizio()
{
	if (iDebug)	
		cout << S_DEBUG << "CentroDiServizio object '" << name << "' destroyed.";
}



void CentroDiServizio::addRouting(CentroDiServizio* cds, double prb)
{
	
	if (!checkVectors())
	{
		printError();
	}
	else
	{
		destinations.push_back(cds);
		probabilities.push_back(prb);
		rangeStart.push_back(0.0);
		rangeStop.push_back(0.0);
		
		if (iDebug)	
			cout << S_DEBUG << "CentroDiServizio '" << name << "' has " << destinations.size() << " routings now.";
		
		double tot=0.0;
		for (int i=0; i<destinations.size(); i++)
		{
			tot += probabilities.at(i);
			if (iDebug)	
				cout << S_DEBUG << "Tot is : " << tot << " at iteration " << i+1;
		}
		
		double min;
		for (int i=0; i<destinations.size(); i++)
		{
			if (i==0)
			{
				min = 0.0;
			}
			else
			{
				min = rangeStop.at(i-1);		
			}
			rangeStart.at(i) = min;
			rangeStop.at(i) = min + probabilities.at(i)/tot;
		}
	}
}



void CentroDiServizio::printRoutings()
{
	if (!checkVectors())
	{
		printError();
	}
	else
	{
		cout.setf(ios::left, ios::adjustfield);
		
		cout << S_PRINT << "Printing Routings for CentroDiServizio '" << name << "'.";
		
		cout << S_PRINT;
		cout << setw(15) << setfill(' ') << "Destination";
		cout << setw(15) << setfill(' ') << "Probability";
		cout << setw(15) << setfill(' ') << "Start";
		cout << setw(15) << setfill(' ') << "Stop";
		
		for (int i=0; i<destinations.size(); i++)
		{
			CentroDiServizio* tmp = destinations.at(i);
			cout << S_PRINT;
			cout << setw(15) << setfill(' ') << tmp->getName();
			cout << setw(15) << setfill(' ') << probabilities.at(i);
			cout << setw(15) << setfill(' ') << rangeStart.at(i);
			cout << setw(15) << setfill(' ') << rangeStop.at(i);
		}
	}
	cout << endl;
}


string CentroDiServizio::getName()
{
	return name;
}
double CentroDiServizio::getDistrParam()
{
	return dDistrParam;
}


CentroDiServizio* CentroDiServizio::getNextRoute(double rnd)
{
	CentroDiServizio* ret=0;
	
	/*
PRINT :: Printing Routings for CentroDiServizio 'CPU'.
PRINT :: Destination    Probability    Start          Stop           
PRINT :: Disk1          0.701754       0              0.701754       
PRINT :: Disk2          0.263158       0.701754       0.964912       
PRINT :: Terminali      0.035088       0.964912       1            

	printRoutings() ha lo STESSO loop e lo stesso accesso di getNextRoute(double):
	for (int i=0; i<destinations.size(); i++)
	{
		CentroDiServizio* tmp = destinations.at(i);
		...
	}
	
	 */
	
	
	// SAY WHAT?
	//if (rnd==1.0) rnd=1.001;
	
	if (iDebug)	
			cout << S_DEBUG << "Choosing route with value of '" << rnd << "'.";
	
	for (int i=0; i<destinations.size(); i++)
	{
		CentroDiServizio* tmp = destinations.at(i);
		
		// SAY WHAT?
		//if (rnd>=rangeStart.at(i) && rnd<rangeStop.at(i))
		if (rnd>=rangeStart.at(i) && rnd<=rangeStop.at(i))
		{
			if (iDebug)	
			cout << S_DEBUG << "\tRoute to '" << tmp->getName() << "' WAS choosen.";
			ret = tmp;
		}
		else 
		{
			if (iDebug)	
			cout << S_DEBUG << "\tRoute to '" << tmp->getName() << "' not choosen.";
		}	
	}
	
	return ret;
}


long CentroDiServizio::getNextEventClock()
{
	if (iDebug)	
			cout << S_DEBUG << "Executing virtual function CentroDiServizio::getNextEventClock";
	long clk = rand();
	return clk;
}




int CentroDiServizio::checkVectors()
{
	return (destinations.size()==probabilities.size() 
				&& destinations.size()==rangeStart.size() 
				&& destinations.size()==rangeStop.size());
}
void CentroDiServizio::printError()
{
	cout << S_ALERT << "CentroDiServizio object '" << name << "' has WRONG ROUTING vector sizes.";
	cout << S_ALERT << "\tDestinations  : " << destinations.size();
	cout << S_ALERT << "\tProbabilities : " << probabilities.size();
	cout << S_ALERT << "\tRangeStart    : " << rangeStart.size();
	cout << S_ALERT << "\tRangeStop     : " << rangeStop.size();
}


void CentroDiServizio::registerAsSource()
{
	if (iDebug)	
			cout << S_DEBUG << "Executing virtual function CentroDiServizio::registerAsSource";
}
void CentroDiServizio::registerAsDestination()
{
	if (iDebug)	
			cout << S_DEBUG << "Executing virtual function CentroDiServizio::registerAsDestination";
}


long CentroDiServizio::getQueuedOrWaiting()
{
	//if (iDebug)	
	//		cout << S_DEBUG << "Executing function CentroDiServizio::getQueuedOrWaiting";
	
	return iQueued;
}


int CentroDiServizio::isTerminalOrCpuDisk()
{
	return isTerminal;
}
