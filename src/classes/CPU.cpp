#include <iostream>
#include <string>

using namespace std;

#include "../headers/CPU.h"

#include "../constants.cpp"



CPU::CPU(int iDbg, string n, double distrParam, double distrSubParam)
{
	isTerminal = 0;
	
	dDistrParam = distrParam;
	dDistrSubParam = distrSubParam;
	
	iDebug = iDbg;
	name = n;
	
	if (iDebug)	
		cout << S_DEBUG << "CPU object '" << name << "' created.";
}
CPU::~CPU()
{
	if (iDebug)	
		cout << S_DEBUG << "CPU object '" << name << "' destroyed.";
}


void CPU::manageEvent()
{
	if (iDebug)	
		cout << S_DEBUG << "CPU object '" << name << "' managing event 'End of " << name << " Processing'" << endl;
}


long CPU::getNextEventClock()
{
	if (iDebug)	
			cout << S_DEBUG << "Executing virtual function CPU::getNextEventClock";
	
	double rnd = getRandomIperExponential(dDistrParam, dDistrSubParam);
	
	long clk = getRandomToClockScaled(rnd);
	
	return clk;
}


void CPU::registerAsSource()
{
	//if (iDebug)
	//		cout << S_DEBUG << "Executing virtual function CPU::registerAsSource";
	iQueued--;
}
void CPU::registerAsDestination()
{
	//if (iDebug)
	//		cout << S_DEBUG << "Executing virtual function CPU::registerAsDestination";
	iQueued++;
}
