#include <iostream>
#include <string>
#include <cstring>

using namespace std;

#include "constants.cpp"

#include "classes/Simulazione.cpp"

#include "classes/CentroDiServizio.cpp"
#include "classes/CPU.cpp"
#include "classes/Disk.cpp"
#include "classes/Terminale.cpp"

#include "classes/Calendario.cpp"
#include "classes/Evento.cpp"

#include "classes/Misure.cpp"

int main(int argc, char *argv[]) {
	//cout << "\nAll arguments passed (" << argc << ") :\n\t" << *argv;

	int iUsers = 0;
	int iMaxEvents = 0;
	char cDebug = 'n';
	int iDebug = 0;

	if (argc <= 2) {
		cout << S_ALERT << S_WARN11;
		cout << S_ALERT << S_WARN12;
		cout << endl;
		exit(1);
	} else {

		iUsers = atoi(argv[1]);
		iMaxEvents = atoi(argv[2]);

		if (iUsers == 0 || iMaxEvents == 0) {
			cout << S_ALERT << S_WARN91;
			cout << S_ALERT << S_WARN92;
			cout << endl;
			exit(1);
		}

		if (iUsers >= I_PARAM_MIN_USERS && iUsers <= I_PARAM_MAX_USERS) {
			cout << S_PRINT << S_INFO01 << iUsers << endl;
		} else {
			cout << S_ALERT << S_WARN01 << iUsers;
			cout << S_ALERT << S_WARN02 << I_PARAM_MIN_USERS << S_WARN03
					<< I_PARAM_MAX_USERS << S_WARN04;
			cout << endl;
			exit(1);
		}

		if (iMaxEvents >= I_PARAM_MIN_EVENTS
				&& iMaxEvents <= I_PARAM_MAX_EVENTS) {
			cout << S_PRINT << S_INFO11 << iMaxEvents;
			cout << endl;
		} else {
			if (iMaxEvents < I_PARAM_MIN_EVENTS) {
				cout << S_ALERT << S_WARN11 << iMaxEvents;
				cout << S_ALERT << S_WARN12 << I_PARAM_MIN_EVENTS;
				cout << endl;

				iMaxEvents = I_PARAM_MIN_EVENTS;
			}
			if (iMaxEvents > I_PARAM_MAX_EVENTS) {
				cout << S_ALERT << S_WARN11 << iMaxEvents;
				cout << S_ALERT << S_WARN13 << I_PARAM_MAX_EVENTS;
				cout << endl;

				iMaxEvents = I_PARAM_MAX_EVENTS;
			}

			cout << S_PRINT << S_INFO11 << iMaxEvents;
			cout << endl;
		}

		if (argc <= 3) {
			cout << "\nDo you want to debug (y/N) : ";
			cin >> cDebug;
			if (cDebug == 'y' || cDebug == 'Y')
				iDebug = 1;
		} else {

			if (strncmp(argv[3], S_PARAM_DEBUG, I_PARAM_DEBUG_LEN) == 0) {
				iDebug = 1;
			} else {
				if (strncmp(argv[3], S_PARAM_NODEBUG, I_PARAM_NODEBUG_LEN)
						== 0) {
					iDebug = 0;
				} else {
					cout << S_ALERT << S_WARN91;
					cout << S_ALERT << S_WARN92;
					cout << endl;
					exit(1);
				}
			}
		}
	}

	unsigned int seed = time(0);
	cout << S_DEBUG << "Doing srand() with seed : " << seed << endl;
	srand(seed);

	cout << S_DEBUG << "RAND_MAX : " << RAND_MAX;
	cout << S_DEBUG << "DOUBLE MAX : " << __DBL_MAX__;
	cout << S_DEBUG << "DOUBLE MIN : " << __DBL_MIN__;
	cout << S_DEBUG << "Clocks per Millisec : " << getClocksScaledPerMillisec();
	cout << endl;

	Simulazione *simulazione = new Simulazione(iDebug, iUsers, iMaxEvents);

	// TODO : remove this test implementation

	//this is ok: simulazione->testNextRoute()
	//simulazione->testNextRoute();

	//this is ok: simulazione->testMisure()
	//simulazione->testMisure();

	//this is ok : simulazione->testCalendario();
	//simulazione->testCalendario();

	//this is ok : simulazione->testDistros();
	//simulazione->testDistros();

	//this SEEMS ok: simulazione->testEventi()
	simulazione->testEventi();

	// TODO : end remove...

	return 0;
}	// end main
