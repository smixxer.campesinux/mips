#ifndef CONSTANTS_CPP_
#define CONSTANTS_CPP_

#include <math.h>



static string S_DEBUG = "\nDEBUG :: ";
static string S_PRINT = "\nPRINT :: ";
static string S_TESTS = "\nTESTS :: ";
static string S_ALERT = "\nALERT :: ";


static string S_INFO01 = "Running simulation with number of users = ";
static int I_PARAM_MIN_USERS = 5;
static int I_PARAM_MAX_USERS = 60;
static string S_WARN01 = "Wrong number of users : ";
static string S_WARN02 = "Allowed range is (";
static string S_WARN03 = " , ";
static string S_WARN04 = ")";

static string S_INFO11 = "Running simulation with max number of events = ";
static int I_PARAM_MIN_EVENTS = 10000;
static int I_PARAM_MAX_EVENTS = 15000;
static string S_WARN11 = "Wrong number of max events : ";
static string S_WARN12 = "Limit was upsized to ";
static string S_WARN13 = "Limit was downsized to ";

static string S_WARN91 = "Wrong command line parameters.";
static string S_WARN92 = "Usage : main users maxEvents [-debug|-noDebug]\n";
static char* S_PARAM_DEBUG = "-debug";
static int I_PARAM_DEBUG_LEN = 6;
static char* S_PARAM_NODEBUG = "-noDebug";
static int I_PARAM_NODEBUG_LEN = 8;


static string S_CDS_CPU = "CPU";
static double D_CDS_MILLISECS_PER_JOB_CPU = 39.364;
static double D_CDS_DISTRO_PARAM2_CPU = 0.3;

static string S_CDS_Disk1 = "Disk1";
static double D_CDS_MILLISECS_PER_JOB_Disk1 = 12.5;
static double D_CDS_DISTRO_PARAM2_Disk1 = 3.0;

static string S_CDS_Disk2 = "Disk2";
static double D_CDS_MILLISECS_PER_JOB_Disk2 = 50.0;
static double D_CDS_DISTRO_PARAM2_Disk2 = 2.0;

static string S_CDS_Terminale = "Terminali";
static double D_CDS_MILLISECS_PER_JOB_Terminale = 12000.0;
static double D_CDS_DISTRO_PARAM2_Terminale = 0.0;


static string S_MISURE_RISPOSTA = "Tempo Complessivo di Risposta";
static string S_MISURE_ATTESA = "Tempo di Attesa in Coda";


static string S_EVENT_NO_PARENT = "NoParent";
static string S_EVENT_NO_PARENT_SHORT = "NoFK";


static long CLOCKS_PER_MILLISECOND_RATE = 796107;
static long CLOCK_MAX_RAND_MAX_FACTOR = 4096;

static long RAND_PRECISION_FACTOR_LONG = 99999999;
static double RAND_PRECISION_FACTOR_DOUBLE = 99999999.9;



inline double getRandomForRoute()
{
	double rnd = rand() % 1000 / 1000.0 ;
	return rnd;
}



inline double getRandomUniform()
{
	double rnd = rand() % RAND_PRECISION_FACTOR_LONG / RAND_PRECISION_FACTOR_DOUBLE ;
	
	return rnd;
}
inline double getRandomExponential(double average)
{
	double ret= 0.0;
	
	double uni = getRandomUniform();
	
	if (uni<__DBL_MIN__)
	{
		uni = __DBL_MIN__;
	}
	
	ret = (-1 * average) * log(uni);
	
	return ret;
}
inline double getRandomKappaErlang(double average, int k)
{
	double ret= 0.0;
	
	for (int i=1; i<=k; i++)
	{
		double exp = getRandomExponential(average/k);

		ret += exp;
	}
	
	return ret;
}
inline double getRandomIperExponential(double average, double prob)
{
	double ret= 0.0;
	
	double exp = getRandomExponential(1.0);
	double uni = getRandomUniform();
	
	if (uni<=prob)
	{
		ret = (average / (2*prob)) * exp;
	}
	else
	{
		ret = (average / (2*(1-prob))) * exp;
	}
	
	if (ret>__DBL_MAX__)
	{
		ret=__DBL_MAX__;
	}
	
	return ret;
}


inline long getRandomToClockScaled (double rnd)
{
	long clk = (long) (rnd*(CLOCKS_PER_MILLISECOND_RATE/CLOCK_MAX_RAND_MAX_FACTOR));
	
	return clk;
}
inline double getClockScaledToMilliSecs (long clk)
{
	double millis = ( (double)clk ) / ( CLOCKS_PER_MILLISECOND_RATE/CLOCK_MAX_RAND_MAX_FACTOR );
	
	return millis;
}
// overloading per non fare casini se la media/varianza/dev.std sono troppo "grossa" (sono calcolata come double)
inline double getClockScaledToMilliSecs (double clk)
{
	double millis = (clk) / ( CLOCKS_PER_MILLISECOND_RATE/CLOCK_MAX_RAND_MAX_FACTOR );
	
	return millis;
}
inline long getClocksScaledPerMillisec()
{
	long clk = (long) (CLOCKS_PER_MILLISECOND_RATE/CLOCK_MAX_RAND_MAX_FACTOR);
	
	return clk;
}



inline long getForTestRandomMisura()
{
	long rnd = rand() % 1000000;
	return rnd;
}


#endif /*CONSTANTS_CPP_*/
