#ifndef TERMINALE_H_
#define TERMINALE_H_

#include <string>

using namespace std;

#include "CentroDiServizio.h"



class Terminale : public CentroDiServizio
{
	
public:
	Terminale(int=0, string="Terminale", double=0, double=0);
	virtual ~Terminale();
	
	void manageEvent();
	
	virtual long getNextEventClock();
	
	virtual void registerAsDestination();
	virtual void registerAsSource();
	
};

#endif /*TERMINALE_H_*/
