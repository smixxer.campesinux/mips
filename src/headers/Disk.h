#ifndef DISK_H_
#define DISK_H_

#include <string>

using namespace std;

#include "CentroDiServizio.h"



class Disk : public CentroDiServizio
{
	
public:
	Disk(int=0, string="Disk", double=0, double=0);
	virtual ~Disk();
	
	void manageEvent();
	
	virtual long getNextEventClock();
	
	virtual void registerAsDestination();
	virtual void registerAsSource();
	
};

#endif /*DISK_H_*/
