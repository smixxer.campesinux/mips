#ifndef MISURA_H_
#define MISURA_H_



// TODO : verificare se vale la pena/e' possibile fare un Template di classe...
// TODO : per ora si usa LONG per il valore della misura, visto che devo misurare:
// - il tempo di attesa in coda (differenza di "clock")
// - il tempo totale di esecuzione (sommatoria di "clock")
class Misura
{

public:
	Misura(long=0, long=0);
	~Misura();
	
	long getClock();
	long getValue();


private:
	long clock;
	long value;
};

#endif /*MISURA_H_*/
