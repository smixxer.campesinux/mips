#ifndef CPU_H_
#define CPU_H_

#include <string>

using namespace std;

#include "CentroDiServizio.h"



class CPU : public CentroDiServizio
{
	
public:
	CPU(int=0, string="CPU", double=0, double=0);
	virtual ~CPU();
	
	void manageEvent();
	
	virtual long getNextEventClock();
	
	virtual void registerAsDestination();
	virtual void registerAsSource();
	
};

#endif /*CPU_H_*/
