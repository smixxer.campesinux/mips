#ifndef CENTRODISERVIZIO_H_
#define CENTRODISERVIZIO_H_

#include <string>
#include <vector>

using namespace std;



class CentroDiServizio
{
	
public:
	CentroDiServizio(int=0, string="CentroDiServizio", double=0, double=0);
	virtual ~CentroDiServizio();
	
	void addRouting(CentroDiServizio*, double);
	void printRoutings();
	
	CentroDiServizio* getNextRoute(double);
	virtual long getNextEventClock();
	
	virtual void registerAsDestination();
	virtual void registerAsSource();
	
	int isTerminalOrCpuDisk();
	
	long getQueuedOrWaiting();
	
	double getDistrParam();
	string getName();
	
protected:
	double dDistrParam;
	double dDistrSubParam;

	int iDebug;
	long iQueued;
	
	string name;
	int isTerminal;
	
private:
	int checkVectors();
	void printError();
	
	vector<CentroDiServizio*> destinations;
	vector<double> probabilities;
	vector<double> rangeStart;
	vector<double> rangeStop;	
};

#endif /*CENTRODISERVIZIO_H_*/
