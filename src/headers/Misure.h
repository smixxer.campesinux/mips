#ifndef MISURE_H_
#define MISURE_H_

#include <string>
#include <queue>

using namespace std;

#include "Misura.h"
#include "CentroDiServizio.h"



// TODO : verify if to switch from <queue> to <vector>
class Misure
{

public:
	Misure(CentroDiServizio*, string, int=0);
	~Misure();
	
	Misura getNext();
	void setNext(Misura);
	
	long getSize();
	string getName();
	
	void setEdge(long);
	
	CentroDiServizio* getCds();
	
	int isOnCds(CentroDiServizio*);
	
	void printMisure();
	
	// TODO : remove these test methods
	void testMisure(long);
	// TODO : end remove...
	
	
private:
	int iDebug;
	
	CentroDiServizio* cds;
	
	queue<Misura> misure; 
	
	string measureName;
	
	long lEdge;
	
	long count;
};

#endif /*MISURE_H_*/
