#ifndef SIMULAZIONE_H_
#define SIMULAZIONE_H_

#include "CentroDiServizio.h"
#include "CPU.h"
#include "Disk.h"
#include "Terminale.h"

#include "Misure.h"

#include "Calendario.h"

#include "../constants.cpp"



class Simulazione
{
	
public:
	Simulazione(int=0, int=0, long=I_PARAM_MIN_EVENTS);
	~Simulazione();
	
	// TODO : remove these test methods
	void testNextRoute();
	void testMisure();
	void testCalendario();
	void testDistros();
	void testDistro(CentroDiServizio*, string);
	void testEventi();
	// TODO : end remove...
	
	Misure* getMisureAttesa();
	Misure* getMisureRisposta();
	
	void setEdge(long);
	
	
private:
	int iDebug;
	int iUsers;
	long lMaxEvents;
	
	CPU* cpu;
	Disk* disk1;
	Disk* disk2;
	Terminale* terminale;
	
	Misure* misureAttesa;
	Misure* misureRisposta;
	
	Calendario* calendario;
	
	void setupCdsAndRouting();
	void setupMisure();
	void setupCalendario();
	
};

#endif /*SIMULAZIONE_H_*/
