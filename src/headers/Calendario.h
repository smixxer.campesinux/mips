#ifndef CALENDARIO_H_
#define CALENDARIO_H_

#include <vector>

using namespace std;

#include "Evento.h"



// early definition for inner object
class Simulazione;



class Calendario
{
	
public:
	Calendario(Simulazione*, int=0);
	
	~Calendario();

	// TODO : remove these test methods
	void testCalendario(long, CentroDiServizio*);
	void testEventi(int, long, CentroDiServizio*);
	// TODO : end remove...
	
	void addEvent(Evento*);
	Evento* getNextEventToProcess(long, long=0);
	void processEvent(Evento* evt, int=0);
	
	void printCalendario();
	void printCalendarioHeader();
	
	
private:
	Simulazione* simulaz;
	int iDebug;
	
	long currClock;
	vector<Evento> eventi;
	
	long getNewEventId();
	
	long getWhenWillBeFree(CentroDiServizio*, Evento*, int=0);
	
	long getClockFromRootEvent(Evento*);
	
	long getLastEventClock();
};

#endif /*CALENDARIO_H_*/
