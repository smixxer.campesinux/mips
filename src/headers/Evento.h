#ifndef EVENTO_H_
#define EVENTO_H_

#include "CentroDiServizio.h"



class Evento
{
	
public:
	Evento(CentroDiServizio* =0, CentroDiServizio* =0, Evento* =0, long=0, long=0);
	Evento(const Evento&);
	virtual ~Evento();
	
	CentroDiServizio* getSource();
	CentroDiServizio* getDestination();
	
	Evento* getParent();
	void setParent(Evento*);
	
	long getClock();
	void raiseClock();
	
	long getId();
	void setId(long);
	
	static void printEventHeader();
	void printEvent(long=0);
	void printEventShort(long=0);
	
	
private:
	CentroDiServizio* source;
	CentroDiServizio* destination;
	
	long id;
	Evento* parent;
	
	long clock;
};

#endif /*EVENTO_H_*/

